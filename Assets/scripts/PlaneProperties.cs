﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneProperties : MonoBehaviour {

    //the properties for every plane
	public Rigidbody rb;
	public float thrust;
	public float maxSpeed;
	public float agility;
	public float maxTurnSpeed;
	public float revertToRotlessSpeed;
	public float straightenForce;
	public float wingArea;
	public float breakKeepPercentagePerSec;

}
