﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaneControls : MonoBehaviour {

	[SerializeField]
	PlaneProperties plane;

	Rigidbody rb;

	[SerializeField]
	float rotSpeed;

	[SerializeField]
	Text throttleText;

	[SerializeField]
    Text speedText;

	[SerializeField]
	float throttleSpeed;
    
	float throttle = 0.0f;

	float gravity = 9.81f;

	void Awake()
	{
		rb = plane.rb;
	}

	void FixedUpdate()
	{

		DoMovement();

	}

	void DoMovement()
	{

		//the local velocities
		Vector3 localVelocity = rb.transform.InverseTransformDirection(rb.velocity);
		Vector3 localAngVelocity = rb.transform.InverseTransformVector(rb.angularVelocity);
        
		//stabilise the plane (slow down rotation)
		float revertness = plane.revertToRotlessSpeed * Time.fixedDeltaTime;
		rb.angularVelocity -= rb.angularVelocity * revertness;

        //change throttle according to input
		if (Input.GetKey(KeyCode.LeftShift))
		{
			throttle += Time.fixedDeltaTime * throttleSpeed;
		}
		else if (Input.GetKey(KeyCode.LeftControl))
		{
			throttle -= Time.fixedDeltaTime * throttleSpeed;
		}

		if (Input.GetKeyDown(KeyCode.X))
		{
			throttle = 0.0f;
		}

		if (throttle > 1.0f)
		{
			throttle = 1.0f;
		}
		else if (throttle < 0.0f)
		{
			throttle = 0.0f;
		}

		//get and apply break input
		if (Input.GetKey(KeyCode.B))
		{
			rb.AddForce(-rb.velocity * plane.breakKeepPercentagePerSec * Time.fixedDeltaTime, ForceMode.VelocityChange);
		}

		//apply gravity
		rb.AddForce(Vector3.down * gravity, ForceMode.Acceleration);

		//calculate and apply y drag
		float yDrag = 0.5f * 1.2f * (localVelocity.y * localVelocity.y) * 1 * plane.wingArea;
		Vector3 dragYUp = rb.transform.TransformDirection(Vector3.up * yDrag);
		Vector3 dragYZ = rb.transform.TransformDirection(Vector3.forward * yDrag);
		float angleZY = Vector3.Angle(Vector3.forward * localVelocity.z, new Vector3(0, localVelocity.y, localVelocity.z));
		float yTransferEffective = 0.0f;
		if (angleZY > 0.0f && angleZY <= 45.0f)
		{
			yTransferEffective = 1.0f;
		}
		else if (angleZY > 45.0f && angleZY <= 90.0f)
		{
			yTransferEffective = ((angleZY - 45) / 45);
		}
		if (localVelocity.y <= 0.0f)
		{
			rb.AddForce(dragYUp, ForceMode.Force);
		}
		else
		{
			rb.AddForce(-dragYUp, ForceMode.Force);
		}
		if (localVelocity.z >= 0.0f)
        {
			rb.AddForce(dragYZ * yTransferEffective * (0.65f - (localVelocity.z / plane.maxSpeed / 2.0f)), ForceMode.Force);
        }
        else
        {
			rb.AddForce(-dragYZ * yTransferEffective * (0.65f - (localVelocity.z / plane.maxSpeed / 2.0f)), ForceMode.Force);
        }

        
        //calculate and apply lift
		float maxLift = rb.mass * gravity * 1.2f;
		float speedLiftRatio = localVelocity.z / plane.maxSpeed;
		speedLiftRatio = 1 - speedLiftRatio;
		speedLiftRatio *= speedLiftRatio;
		speedLiftRatio = 1 - speedLiftRatio;
		Vector3 lift = new Vector3(0, speedLiftRatio * maxLift, 0);
		lift = rb.transform.TransformDirection(lift);
		rb.AddForce(lift, ForceMode.Force);
        
        //apply throttle with thrust to plane
		Vector3 force = Vector3.forward * plane.thrust * throttle;
		force = rb.transform.TransformDirection(force);
		rb.AddForce(force, ForceMode.Force);

        //apply the z drag
		float ratio = localVelocity.z / plane.maxSpeed;
        float drag = ratio * ratio * plane.thrust;
		force = Vector3.zero;
		if (localVelocity.z >= 0.0f)
        {
            force.z = -drag;
        }
        else
        {
            force.z = drag;
        }
		force = rb.transform.TransformDirection(force);
		rb.AddForce(force, ForceMode.Force);
        
        //change throttle and speed UI texts
		throttleText.text = "" + Mathf.RoundToInt(throttle * 100.0f) + "%";
		float v = Mathf.Round(rb.velocity.magnitude * 10) / 10;
		if (Mathf.RoundToInt(v * 10) % 10 == 0)
		{
			speedText.text = "" + (Mathf.RoundToInt(v)) + ".0m/s";
		}
		else
		{
			speedText.text = "" + (v) + "m/s";
		}
        
        //get and process rotational input
		Vector3 input = Vector3.zero;
		input.x += Input.GetAxis("Vertical") * rotSpeed;
		input.z -= Input.GetAxis("Horizontal") * rotSpeed;
		input.y += Input.GetKey(KeyCode.Q) ? 0.0f : 1.0f;
		input.y += Input.GetKey(KeyCode.E) ? 0.0f : -1.0f;

		float straightForce = (localVelocity.y * localVelocity.y) * plane.straightenForce / 800;
		if (localVelocity.y > 0.0f)
		{
			straightForce *= -1;
		}
		rb.AddRelativeTorque(new Vector3(straightForce, localVelocity.x * plane.straightenForce, 0), ForceMode.Acceleration);
        
		if (plane.maxTurnSpeed != 0.0f)
		{
			
			if (input.x >= 0.0f)
			{
				input.x *= 1.0f - (localAngVelocity.x / plane.maxTurnSpeed);
			}
			else
			{
				input.x *= 1.0f - (-localAngVelocity.x / plane.maxTurnSpeed);
			}

			if (input.y >= 0.0f)
            {
				input.y *= 1.0f - (localAngVelocity.y / plane.maxTurnSpeed);
            }
            else
            {
				input.y *= 1.0f - (-localAngVelocity.y / plane.maxTurnSpeed);
            }
            
			if (input.z >= 0.0f)
            {
				input.z *= 1.0f - (localAngVelocity.z / plane.maxTurnSpeed);
            }
            else
            {
				input.z *= 1.0f - (-localAngVelocity.z / plane.maxTurnSpeed);
            }

		}
		else
		{
			input = Vector3.zero;
		}

		//apply rotational input
		float rotSpeedRatio = 1 - (localVelocity.z / plane.maxSpeed);
		rotSpeedRatio *= rotSpeedRatio;
		rotSpeedRatio = (1 - rotSpeedRatio) * 1.25f;
		input *= plane.agility * rotSpeedRatio;
		rb.AddRelativeTorque(input, ForceMode.Acceleration);

	}

}
