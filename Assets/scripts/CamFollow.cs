﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour {

	[SerializeField]
	float speed;

	[SerializeField]
	Transform camPos;

	void Start () {
		
	}

	void FixedUpdate () {
        
		//move the cam to the target pos (camPos) with the given speed
		transform.position = Vector3.Lerp(transform.position, camPos.position, Time.fixedDeltaTime * speed);
		transform.rotation = Quaternion.Lerp(transform.rotation, camPos.rotation, Time.fixedDeltaTime * speed);

	}

}
