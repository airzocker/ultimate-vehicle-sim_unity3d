# Everyone can contribute to this project with the following requirements:

  * Only english code comments, commit messages and gitlab comments, please!

  * Just clone this repo (it's going to be the root directory of the unity
    project) and open this project in Unity3d once.

  * The project has to have External Version Control Support enabled in Unity3d
    (Edit->Project Settings->Editor, select Visible Meta Files)

  * The Feature Branch Workflow is used, master is only the stable branch,
    experimental is the ~working development branch and everyone makes their
    own feature branches with pull requests when they are finished or need
    he

  * Please add simple comments to your scripts, so others can understand your work.

  * The Unity3d version is Unity-2018.2.0f2 (but maybe works with slightly
    different version).

  * Images, audio, models and other binaries go into the corresponding folder
    `/Assets/resources/<correspondingFolder>/*`!

  * Image sources, audio sources, model sources and similar can be committed
    to the "ultimate-vehicle-sim_ext-sources" repo
    (https://gitlab.com/airzocker/ultimate-vehicle-sim_ext-sources), being
    one of the following source file types:
    * .xcf (gimp; images)
    * .mmpz (lmms; audio) with all the used audio files in the same directory
    * .blend (blender; models)
    * other free file formats for sources can be requested
    
  * The following assets have to be installed from the Unity Asset Store into
    the ignored `/Assets/assetPacks/*` (!!!) folder:
    * Standard Assets (https://assetstore.unity.com/packages/essentials/asset-packs/standard-assets-32351)

  * Having fun!

# ToDo:

  * The current main scene to work on is `/Assets/scenes/level1.unity`.

  * Create basic model(s)
  
  * Create basic map